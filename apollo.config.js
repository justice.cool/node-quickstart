module.exports = {
    client: {
      service: {
          url: "https://api.justice.cool/v1",
      },
      includes: [
          "**/*.{ts,tsx,js,jsx,graphql,gql}"
      ],
      excludes: [
          "node_modules/**/*"
      ],
    },
  };