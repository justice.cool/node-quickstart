import {JCoolApi, Env} from '@justice.cool/api-wrapper';
import gql from 'graphql-tag';

// build your api instance
const api = new JCoolApi('<my api key>', {}, Env.prod);

(async () => {
    // sample query
    //   nb  -  install the Apollo GraphQL vscode extension to have auto-completion here !
    const {disputes} = await api.client.get(gql`query MyQuery { disputes { id } }`);
    for (const {id} of disputes) {
        // do something with id
        console.log('My dispute: ' + id);
    }

})().catch(e => {
    console.error('Query error', e);
    debugger;
});