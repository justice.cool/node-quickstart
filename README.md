A quick-start for the [Justice.cool API](https://docs.justice.cool/)

Installation:

1) Grab [a PROD api key](https://app.justice.cool/dev)  or [a TEST api key](https://app.staging.justice.cool/dev)


2) Clone & open the repo
```bash
git clone git@gitlab.com:justice.cool/node-quickstart.git
cd node-quickstart
npm i
code .
```

3) Then fill in your API in line 5, and you're all set, ready to press F5 !

**NB:** You can optionally install the [Apollo GraphQL vscode extension](https://marketplace.visualstudio.com/items?itemName=apollographql.vscode-apollo) to get autocompletion whyile typing requests !